package com.example.myapplication


object GeoJsonViewerConstants {
    var INTENT_EXTRA_JSON_URI = "json-uri"
    var INTENT_EXTRA_JSON_COLORS = "json-colors"
    var APP_TAG = "geojsonviewer"
}