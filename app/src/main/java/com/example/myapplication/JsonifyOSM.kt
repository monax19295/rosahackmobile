package com.example.myapplication

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.view.View
import org.osmdroid.bonuspack.kml.KmlDocument
import org.osmdroid.bonuspack.kml.Style
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.FolderOverlay
import java.io.IOException
import android.opengl.ETC1.getHeight
import android.opengl.ETC1.getWidth
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable


class JsonifyOSM {

    companion object{
        @Throws(IOException::class)
        fun geoJsonifyMap(
            map: MapView,
            json: String,
            context: Context
        ) {


            fun convert(drawable: Drawable): Bitmap?{
                try {
                    val bitmap: Bitmap

                    bitmap = Bitmap.createBitmap(
                        drawable.getIntrinsicWidth(),
                        drawable.getIntrinsicHeight(),
                        Bitmap.Config.ARGB_8888
                    )

                    val canvas = Canvas(bitmap)
                    drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight())
                    drawable.draw(canvas)
                    return bitmap
                } catch (e: OutOfMemoryError) {
                    // Handle the error
                    return null
                }

            }

            val kmlDocument = KmlDocument()

                kmlDocument.parseGeoJSON(json)

            val defaultMarker = context.resources.getDrawable(R.drawable.ic_opened_door_aperture)
            val defaultBitmap = convert(defaultMarker)
            val defaultStyle = Style(defaultBitmap, Color.RED, 2f, 0x00000000)
            val geoJsonOverlay = kmlDocument.mKmlRoot.buildOverlay(
                map,
                defaultStyle,
                null,
                kmlDocument
            ) as FolderOverlay

            map.overlays.add(geoJsonOverlay)
            map.invalidate()


            // Workaround for osmdroid issue
            // See: https://github.com/osmdroid/osmdroid/issues/337
            map.addOnFirstLayoutListener { v, left, top, right, bottom ->
                val boundingBox = kmlDocument.mKmlRoot.boundingBox
                // Yep, it's called 2 times. Another workaround for zoomToBoundingBox.
                // See: https://github.com/osmdroid/osmdroid/issues/236#issuecomment-257061630
                map.zoomToBoundingBox(boundingBox, false)
                map.zoomToBoundingBox(boundingBox, false)
                map.invalidate()
            }


        }
    }
}