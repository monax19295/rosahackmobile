package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Context
import android.content.Intent
import android.widget.ListView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.TextView
import com.example.myapplication.api.ApiService
import com.example.myapplication.model.HistoryModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import android.widget.Spinner
import android.R.attr.data
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import android.widget.AdapterView.OnItemSelectedListener
import com.example.myapplication.model.PointModel
import com.example.myapplication.model.RouteModel


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        val txtView = findViewById<TextView>(R.id.transport_name)
//        txtView.setText("Тележка Б1")

        // адаптер


        val btn = findViewById<Button>(R.id.letsgo)
        btn.setOnClickListener {
            if(Storage.instance().curTransport == null ||  Storage.instance().curRoute == null) {
                return@setOnClickListener
            }
            Storage.instance().listOfScannedDetails.keys.forEach {
                        ApiService.instance().addHistory(
                            HistoryModel(
                                id = UUID.randomUUID(),
                                user = Storage.instance().curUser,
                                transportId = Storage.instance().curTransport!!.id,
                                detailId = it,
                                location = mapOf("lat" to "57.124124", "lng" to "57.124124"),
                                description = mapOf("title" to "Отправка деталей"),
                                routeId = Storage.instance().curRoute!!.id
                            )
                        ).subscribeOn(Schedulers.io()).subscribe {
                            it
                        }
                    }
            val mapAct = Intent(this, MapActivity::class.java)
            startActivity(mapAct)
        }

//        val mapAct = Intent(this, MapActivity::class.java)
//        startActivity(mapAct)

//        val scanAct = Intent(this, ScannerActivity::class.java)
//        startActivity(scanAct)

//        val nfcAct = Intent(this, NfcScannerActivity::class.java)
//        startActivity(nfcAct)

//        val fragmentManager = supportFragmentManager
//        val fragmentTransaction = fragmentManager.beginTransaction()
//
//        val fragment = MapActivity()
//        fragmentTransaction.add(R.id.fragment_container, fragment)
//        fragmentTransaction.commit()

//        createListView(Storage.instance().listOfScannedDetails.map { it.name }.toTypedArray())

        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            val scanAct = Intent(this, ScannerActivity::class.java)
            startActivity(scanAct)
        }
    }

    fun fixDoors(r: RouteModel): RouteModel{
        val newDoors = mutableListOf<PointModel>()
        r.route.path?.first()?.let { newDoors.add(it) }
        r.route.doors?.forEach {
            if(r.route.path?.contains(it) == true){
                newDoors.add(it)
            }
        }
        r.route.path?.last()?.let { newDoors.add(it) }
        r.route.doors = newDoors
        return r
    }
    
    override fun onResume() {
        super.onResume()
        if(Storage.instance().listOfScannedDetails.isEmpty())
            return
        // calc the transport by the weight
        ApiService.instance().getTransport().subscribeOn(Schedulers.io()).subscribe { transportList->
            ApiService.instance().getDetail().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe { detailList->
                val listDetails = Storage.instance().listOfScannedDetails.keys
                val maxW = detailList.filter { listDetails.contains(it.id) }
                val d = maxW.maxBy { it.description["weight"] as Double }!!.description["weight"] as Double
                    var i = 0
                transportList.sortedBy { it.weight }.forEach {
                    if(it.weight >= d) {
//                        val txtView = findViewById<TextView>(R.id.transport_name)
//                        txtView.setText("Транспорт "+it.name)

                        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                            transportList.map { it.name }.toTypedArray())
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                        val spinner = findViewById(R.id.spinner) as Spinner
                        spinner.adapter = adapter
                        // заголовок
                        spinner.prompt = "Title"
                        // выделяем элемент
                        spinner.setSelection(i)
//                        spinner.onItemSelectedListener( { adapterView, view, i, l ->
//                            Storage.instance().curTransport = transportList.get(i)
//                        })

                        spinner.onItemSelectedListener = object : OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>, view: View,
                                position: Int, id: Long
                            ) {
                                Storage.instance().curTransport = transportList.get(position)
                            }

                            override fun onNothingSelected(arg0: AdapterView<*>) {}
                        }

                        Storage.instance().curTransport = it
                        return@forEach
                    }
                    i++
                }
            }
        }
        // get the route by detail
        ApiService.instance().getRoutes().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe { routes ->
                // get details
                ApiService.instance().getDetail().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe { detailList ->
                        val details = detailList.filter { Storage.instance().listOfScannedDetails.contains(it.id) }
                        val r = routes.filter {
                                var res = false
                            for (uuid in it.detailTypeIds) {
                                res = details.map { it.detailTypeId }.contains(uuid)
                                if(res) {
                                    break
                                }
                            }
//                            it.detailTypeIds.forEach loop@{uuid->
//                                    res = details.map { it.detailTypeId }.contains(uuid)
//                                    if(res) {
//                                        return@loop
//                                    }
//                                }
                                res
                            }

                        if(r.size > 0) {
                            val fixDoorsRoutes = r.map { fixDoors(it) }
                            val adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                                fixDoorsRoutes.map { it.name }.toTypedArray())
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                            val spinner = findViewById(R.id.spinner_route) as Spinner
                            spinner.adapter = adapter
                            // заголовок
                            spinner.prompt = "Title"
                            // выделяем элемент
                            spinner.setSelection(0)
//                        spinner.onItemSelectedListener( { adapterView, view, i, l ->
//                            Storage.instance().curTransport = transportList.get(i)
//                        })

                            spinner.onItemSelectedListener = object : OnItemSelectedListener {
                                override fun onItemSelected(
                                    parent: AdapterView<*>, view: View,
                                    position: Int, id: Long
                                ) {
                                    Storage.instance().curRoute = fixDoorsRoutes.get(position)
                                    
                                }

                                override fun onNothingSelected(arg0: AdapterView<*>) {}
                            }


//                            Storage.instance().curRoute = r[0]

                        } else {
                            Toast.makeText(this, "Список маршрутов для деталей пуст", Toast.LENGTH_SHORT).show()
                        }
                    }

                // get map
                ApiService.instance().getMaps().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe {
                        Storage.instance().curMap = it.find { it.id == Storage.instance().curRoute?.mapId }
                    }
            }

        createListView(Storage.instance().listOfScannedDetails.values.map { it.name }.toTypedArray())
    }

    fun createListView(values: Array<String>){
        if(values.isEmpty())
            return
        val listview = findViewById<ListView>(R.id.listview)

        val list = ArrayList<String>()
        for (i in values.indices) {
            list.add(values[i])
        }
        val adapter = StableArrayAdapter(
            this,
            android.R.layout.simple_list_item_1, list
        )
        listview.setAdapter(adapter)
    }

    private inner class StableArrayAdapter(
        context: Context, textViewResourceId: Int,
        objects: List<String>
    ) : ArrayAdapter<String>(context, textViewResourceId, objects) {

        internal var mIdMap = HashMap<String, Int>()

        init {
            for (i in objects.indices) {
                mIdMap[objects[i]] = i
            }
        }

        override fun getItemId(position: Int): Long {
            val item = getItem(position)
            return mIdMap.get(item)?.toLong() ?: throw IllegalStateException() as Throwable
        }

        override fun hasStableIds(): Boolean {
            return true
        }
    }
}
