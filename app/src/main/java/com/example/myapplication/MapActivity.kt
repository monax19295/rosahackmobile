package com.example.myapplication

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.nfc.NdefMessage
import android.nfc.NfcAdapter
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.widget.Toast
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Overlay
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay
import java.io.IOException
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.api.ApiService
import com.example.myapplication.model.HistoryModel
import io.reactivex.schedulers.Schedulers
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.Polyline
import java.util.*


class MapActivity : AppCompatActivity() {

    var map : MapView? = null
    var context: Context? = null

    // NFC adapter for checking NFC state in the device
    private var nfcAdapter: NfcAdapter? = null

    // Pending intent for NFC intent foreground dispatch.
    // Used to read all NDEF tags while the app is running in the foreground.
    private var nfcPendingIntent: PendingIntent? = null
    // Optional: filter NDEF tags this app receives through the pending intent.
    //private var nfcIntentFilters: Array<IntentFilter>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        val context = applicationContext
        this.context = context

        val map = findViewById<MapView>(R.id.map)
        this.map = map
        map.setTileSource(TileSourceFactory.MAPNIK)
        map.setMultiTouchControls(true)
        map.setMaxZoomLevel(null)

        val mLocationOverlay = MyLocationNewOverlay(GpsMyLocationProvider(this), map)
        mLocationOverlay.enableMyLocation()
        map.overlays.add(mLocationOverlay)


        map.overlayManager.add(object : Overlay() {

            override fun onSingleTapConfirmed(e: MotionEvent?, mapView: MapView?): Boolean {
                // do what you want
                map.invalidate()
                map.postInvalidate()
                return true
            }
        })

        val mapController = map.getController()
        mapController.setZoom(14.5)
        val startPoint = GeoPoint(55.795994,49.1219473)
        mapController.setCenter(startPoint)
        map.addOnFirstLayoutListener { v, left, top, right, bottom ->
            //            val boundingBox = kmlDocument.mKmlRoot.getBoundingBox()
//            // Yep, it's called 2 times. Another workaround for zoomToBoundingBox.
//            // See: https://github.com/osmdroid/osmdroid/issues/236#issuecomment-257061630
//            map.zoomToBoundingBox(boundingBox, false)
//            map.zoomToBoundingBox(boundingBox, false)
            map.invalidate()
        }

        Configuration.getInstance().userAgentValue = BuildConfig.APPLICATION_ID

        class A: CustomZoomButtonsController.OnZoomListener{
            override fun onVisibilityChanged(b: Boolean) {
                map.invalidate()
            }

            override fun onZoom(b: Boolean) {
                map.invalidate()
            }
        }
        map.zoomController.setOnZoomListener(A())
        val line = Polyline()   //see note below!
        var b = false
        line.setPoints(
            Storage.instance().curRoute?.route?.path?.map {
                GeoPoint(it.lat, it.lng)
            }
        )

        Storage.instance().curRoute?.route?.doors?.map {
            val marker = Marker(map)
            marker.position = GeoPoint(it.lat, it.lng)
            marker.icon = context.getDrawable(R.drawable.ic_order)
            Storage.instance().gateQueue.add(it)
            map.overlayManager.add(marker)
        }

        map.overlayManager.add(line)
        map.invalidate()

        try {
            JsonifyOSM.geoJsonifyMap(
                map,
                Storage.instance().curMap?.map.toString() ?: "",
                context
            )
        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(context,"ERRORRRRR",
                Toast.LENGTH_SHORT
            ).show()
        }

        // Check if NFC is supported and enabled
        nfcAdapter = NfcAdapter.getDefaultAdapter(this)
        logMessage("NFC supported", (nfcAdapter != null).toString())
        logMessage("NFC enabled", (nfcAdapter?.isEnabled).toString())


        // Read all tags when app is running and in the foreground
        // Create a generic PendingIntent that will be deliver to this activity. The NFC stack
        // will fill in the intent with the details of the discovered tag before delivering to
        // this activity.
        nfcPendingIntent = PendingIntent.getActivity(
            this, 0,
            Intent(this, javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0
        )

        // Optional: Setup an intent filter from code for a specific NDEF intent
        // Use this code if you are only interested in a specific intent and don't want to
        // interfere with other NFC tags.
        // In this example, the code is commented out so that we get all NDEF messages,
        // in order to analyze different NDEF-formatted NFC tag contents.
        //val ndef = IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED)
        //ndef.addCategory(Intent.CATEGORY_DEFAULT)
        //ndef.addDataScheme("https")
        //ndef.addDataAuthority("*.andreasjakl.com", null)
        //ndef.addDataPath("/", PatternMatcher.PATTERN_PREFIX)
        // More information: https://stackoverflow.com/questions/30642465/nfc-tag-is-not-discovered-for-action-ndef-discovered-action-even-if-it-contains
        //nfcIntentFilters = arrayOf(ndef)

        if (intent != null) {
            // Check if the app was started via an NDEF intent
            logMessage("Found intent in onCreate", intent.action.toString())
            processIntent(intent)
        }

    }


    override fun onResume() {
        super.onResume()
        // Get all NDEF discovered intents
        // Makes sure the app gets all discovered NDEF messages as long as it's in the foreground.
        nfcAdapter?.enableForegroundDispatch(this, nfcPendingIntent, null, null);
        // Alternative: only get specific HTTP NDEF intent
        //nfcAdapter?.enableForegroundDispatch(this, nfcPendingIntent, nfcIntentFilters, null);
    }

    override fun onPause() {
        super.onPause()
        // Disable foreground dispatch, as this activity is no longer in the foreground
        nfcAdapter?.disableForegroundDispatch(this);
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        logMessage("Found intent in onNewIntent", intent?.action.toString())
        // If we got an intent while the app is running, also check if it's a new NDEF message
        // that was discovered
        if (intent != null) processIntent(intent)
    }

    /**
     * Check if the Intent has the action "ACTION_NDEF_DISCOVERED". If yes, handle it
     * accordingly and parse the NDEF messages.
     * @param checkIntent the intent to parse and handle if it's the right type
     */
    private fun processIntent(checkIntent: Intent) {
        // Check if intent has the action of a discovered NFC tag
        // with NDEF formatted contents
        if(checkIntent.action == NfcAdapter.ACTION_TAG_DISCOVERED && Storage.instance().gateQueue.size > 0){
            val pointGate = Storage.instance().gateQueue.pop()

            Storage.instance().listOfScannedDetails.keys.forEach {
                ApiService.instance().addHistory(
                    HistoryModel(
                        id = UUID.randomUUID(),
                        user = Storage.instance().curUser,
                        transportId = Storage.instance().curTransport!!.id,
                        detailId = it,
                        location = mapOf("lat" to pointGate.lat.toString(), "lng" to pointGate.lng.toString()),
                        description = mapOf("title" to "Проход рамки №" + Storage.instance().gateQueue.size + 15),
                        routeId = Storage.instance().curRoute!!.id
                    )
                ).subscribeOn(Schedulers.io()).subscribe {
                    it
                }
            }

            map?.overlayManager?.forEach {
                if(it is Marker && it.position.latitude == pointGate.lat
                    && it.position.longitude == pointGate.lng ){
                    it.icon = context?.getDrawable(R.drawable.ic_verified_list)
                }
            }
            map?.invalidate()
        }

        if (checkIntent.action == NfcAdapter.ACTION_NDEF_DISCOVERED) {
            logMessage("New NDEF intent", checkIntent.toString())

            // Retrieve the raw NDEF message from the tag
            val rawMessages = checkIntent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)
            logMessage("Raw messages", rawMessages.size.toString())

            // Complete variant: parse NDEF messages
            if (rawMessages != null) {
                val messages = arrayOfNulls<NdefMessage?>(rawMessages.size)// Array<NdefMessage>(rawMessages.size, {})
                for (i in rawMessages.indices) {
                    messages[i] = rawMessages[i] as NdefMessage;
                }
                // Process the messages array.
                processNdefMessages(messages)
            }

            // Simple variant: assume we have 1x URI record
            //if (rawMessages != null && rawMessages.isNotEmpty()) {
            //    val ndefMsg = rawMessages[0] as NdefMessage
            //    if (ndefMsg.records != null && ndefMsg.records.isNotEmpty()) {
            //        val ndefRecord = ndefMsg.records[0]
            //        if (ndefRecord.toUri() != null) {
            //            logMessage("URI detected", ndefRecord.toUri().toString())
            //        } else {
            //            // Other NFC Tags
            //            logMessage("Payload", ndefRecord.payload.contentToString())
            //        }
            //    }
            //}

        }
    }

    /**
     * Parse the NDEF message contents and print these to the on-screen log.
     */
    private fun processNdefMessages(ndefMessages: Array<NdefMessage?>) {
        // Go through all NDEF messages found on the NFC tag
        for (curMsg in ndefMessages) {
            if (curMsg != null) {
                // Print generic information about the NDEF message
                logMessage("Message", curMsg.toString())
                // The NDEF message usually contains 1+ records - print the number of recoreds
                logMessage("Records", curMsg.records.size.toString())

                // Loop through all the records contained in the message
                for (curRecord in curMsg.records) {
                    if (curRecord.toUri() != null) {
                        // URI NDEF Tag
                        logMessage("- URI", curRecord.toUri().toString())
                    } else {
                        // Other NDEF Tags - simply print the payload
                        logMessage("- Contents", curRecord.payload.contentToString())
                    }
                }
            }
        }
    }

    /**
     * Log a message to the debug text view.
     * @param header title text of the message, printed in bold
     * @param text optional parameter containing details about the message. Printed in plain text.
     */
    private fun logMessage(header: String, text: String?) {
        Log.i("TAG", text ?: header)
    }
}
