package com.example.myapplication

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.Overlay
import org.osmdroid.views.overlay.Polyline
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay
import java.io.IOException
import java.lang.IllegalStateException

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [MapFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [MapFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MapFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_map, container, false)

        val context = activity?.baseContext ?: throw IllegalStateException()

        val map = v.findViewById<MapView>(R.id.map)
        map.setTileSource(TileSourceFactory.MAPNIK)
        map.setMultiTouchControls(true)
        map.setMaxZoomLevel(null)

        val mLocationOverlay = MyLocationNewOverlay(GpsMyLocationProvider(context), map)
        mLocationOverlay.enableMyLocation()
        map.overlays.add(mLocationOverlay)


        map.overlayManager.add(object : Overlay() {

            override fun onSingleTapConfirmed(e: MotionEvent?, mapView: MapView?): Boolean {
                // do what you want
                map.invalidate()
                map.postInvalidate()
                return true
            }
        })

        val mapController = map.getController()
        mapController.setZoom(14.5)
        val startPoint = GeoPoint(55.795994,49.1219473)
        mapController.setCenter(startPoint)
        map.addOnFirstLayoutListener { v, left, top, right, bottom ->
            //            val boundingBox = kmlDocument.mKmlRoot.getBoundingBox()
//            // Yep, it's called 2 times. Another workaround for zoomToBoundingBox.
//            // See: https://github.com/osmdroid/osmdroid/issues/236#issuecomment-257061630
//            map.zoomToBoundingBox(boundingBox, false)
//            map.zoomToBoundingBox(boundingBox, false)
            map.invalidate()
        }

        Configuration.getInstance().userAgentValue = BuildConfig.APPLICATION_ID

        class A: CustomZoomButtonsController.OnZoomListener{
            override fun onVisibilityChanged(b: Boolean) {
                map.invalidate()
            }

            override fun onZoom(b: Boolean) {
                map.invalidate()
            }
        }
        map.zoomController.setOnZoomListener(A())
        val line = Polyline()   //see note below!
        var b = false
        line.setPoints(
            Storage.instance().curRoute?.route?.path?.map {
                if(b){
                    val marker = Marker(map)
                    marker.position = GeoPoint(it.lat, it.lng)
                    marker.icon = context.getDrawable(R.drawable.ic_verified_list)
                    map.overlayManager.add(marker)
                }
                b = !b
                GeoPoint(it.lat, it.lng)
            }
        )
        map.overlayManager.add(line)
        map.invalidate()

        try {
            JsonifyOSM.geoJsonifyMap(
                map,
                Storage.instance().curMap?.map.toString(),
                context
            )
        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(context,"ERRORRRRR",
                Toast.LENGTH_SHORT
            ).show()
        }

        return map
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment MapFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            MapFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
