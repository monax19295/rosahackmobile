package com.example.myapplication

import android.Manifest
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.example.myapplication.model.DetailQrCodeModel
import com.google.zxing.BarcodeFormat
import kotlinx.android.synthetic.main.activity_scanner.*
import me.dm7.barcodescanner.zxing.ZXingScannerView
import com.google.zxing.Result
import java.util.*

class ScannerActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {

    companion object {
        private const val MY_CAMERA_REQUEST_CODE = 6515
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scanner)

        val qrCodeScanner = findViewById<ZXingScannerView>(R.id.qrCodeScanner)
        setScannerProperties()
    }

    private fun setScannerProperties() {
        qrCodeScanner.setFormats(listOf(BarcodeFormat.QR_CODE))
        qrCodeScanner.setAutoFocus(true)
        qrCodeScanner.setLaserColor(R.color.colorAccent)
        qrCodeScanner.setMaskColor(R.color.colorAccent)
//        if (Build.MANUFACTURER.equals(HUAWEI, ignoreCase = true))
//            qrCodeScanner.setAspectTolerance(0.5f)
    }

    override fun onResume() {
        super.onResume()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA),
                    MY_CAMERA_REQUEST_CODE)
                return
            }
        }
        qrCodeScanner.startCamera()
        qrCodeScanner.setResultHandler(this)
    }

    override fun handleResult(p0: Result) {
        if (p0 != null) {
            val (id, text) = p0.text.split("|")
            val model = DetailQrCodeModel(UUID.fromString(id), text)
            if(!Storage.instance().listOfScannedDetails.containsKey(model.id)){
                Storage.instance().listOfScannedDetails[model.id] = model
            }
            Toast.makeText(this, "Деталь ${text} отсканена.", Toast.LENGTH_SHORT).show()
        }
        onResume()
    }

    override fun onPause() {
        super.onPause()
        qrCodeScanner.stopCamera()
    }
}
