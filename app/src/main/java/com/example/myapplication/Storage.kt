package com.example.myapplication

import com.example.myapplication.model.*
import java.util.*
import kotlin.collections.HashMap

class Storage private constructor(){

    companion object{
        private var INSTANCE: Storage? = null
        fun instance(): Storage {
            if(INSTANCE == null){
                INSTANCE = Storage()
                val d = Storage()
                INSTANCE = d
                return d
            } else {
                return INSTANCE ?: throw IllegalStateException()
            }
        }
    }

    var curUser = "Такелажник Денис"

    var curTransport: TransportModel? = null

    var curRoute: RouteModel? = null

    var curMap: MapModel? = null

    val storage = HashMap<String, String>()

    var listOfScannedDetails = mutableMapOf<UUID, DetailQrCodeModel>()

    var gateIterator = 0

    var gateQueue = ArrayDeque<PointModel>()
}