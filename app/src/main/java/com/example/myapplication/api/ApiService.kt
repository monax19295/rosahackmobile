package com.example.myapplication.api

import com.example.myapplication.model.*
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST

interface ApiService {

    @POST("/transport/calc")
    fun calcTransport(@Body b:List<DetailQrCodeModel>): Observable<TransportModel>

    @POST("/addHistory")
    fun addHistory(@Body b:HistoryModel): Observable<RoutePolylineModel>

    @POST("/wayroute/checkpoint")
    fun checkPointWayRoute(@Body b: CheckPointModel): Observable<Any?>

    @POST("/getTransport")
    fun getTransport(): Observable<List<TransportModel>>

    @POST("/getDetail")
    fun getDetail(): Observable<List<DetailModel>>

    @POST("/getRoutes")
    fun getRoutes(): Observable<List<RouteModel>>

    @POST("/getMaps")
    fun getMaps(): Observable<List<MapModel>>



    /**
     * Companion object to instance the ApiService
     */
    companion object Factory {
        private var instance: ApiService? = null
        fun instance(): ApiService {
            if(instance != null)
                return instance ?: throw IllegalStateException()
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://192.168.8.102:8080")
                .build()
            val m = retrofit.create(ApiService::class.java)
            instance = m
            return m
        }
    }
}