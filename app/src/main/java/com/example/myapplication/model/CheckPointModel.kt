package com.example.myapplication.model

import java.time.Instant
import java.util.*

data class CheckPointModel (
    val wayrouteId: UUID,
    val point: PointModel,
    val user: String,
    val transport: UUID,
    val date: Instant,
    val detailId: UUID
)