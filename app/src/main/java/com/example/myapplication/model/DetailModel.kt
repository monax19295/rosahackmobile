package com.example.myapplication.model

import java.util.*

data class DetailModel (
    val id: UUID = UUID.randomUUID(),
    val description: Map<String, Any>,
    val detailTypeId: UUID
)