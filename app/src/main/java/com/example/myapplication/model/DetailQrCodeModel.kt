package com.example.myapplication.model

import java.util.*

data class DetailQrCodeModel (
    val id: UUID,
    val name: String
)

