package com.example.myapplication.model

import java.util.*


data class HistoryModel(
    val id: UUID,
    val transportId: UUID,
    val user: String, // hackathon mazafaka
    val detailId: UUID,
    val routeId: UUID,
    val location: Map<String, String>,
    val description: Map<String, String>
)