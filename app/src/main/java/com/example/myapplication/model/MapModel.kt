package com.example.myapplication.model

import com.google.gson.JsonObject
import java.util.*

data class MapModel (
    val id: UUID,
    val name: String,
    val map: JsonObject
)