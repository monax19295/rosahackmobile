package com.example.myapplication.model

import java.util.*
import kotlin.collections.ArrayList

data class RouteModel(
    val id: UUID,
    val transportId: UUID,
    val user: String, // hackathon mazafaka
    val detailTypeIds: ArrayList<UUID>,
    val routeId: UUID,
    val name: String,
    val mapId: UUID,
    val location: Map<String, String>,
    val route: Route,
    val description: Map<String, String>
    )

data class Route(
    val path: List<PointModel>,
    var doors: List<PointModel>
)