package com.example.myapplication.model

data class RoutePolylineModel (
    val polylineDotList: List<PointModel>
)

data class PointModel(
    val lat: Double,
    val lng: Double
)