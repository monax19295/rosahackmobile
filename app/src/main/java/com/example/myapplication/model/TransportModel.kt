package com.example.myapplication.model

import java.util.*

data class TransportModel (
    val id: UUID,
    val name: String,
    val height: Double,
    val width: Double,
    val weight: Double
)